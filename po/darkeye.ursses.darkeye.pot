# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the darkeye.ursses.darkeye package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: darkeye.ursses.darkeye\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-07-12 06:26+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../qml/Components/GenerateInputForSettings.qml:60
msgid "Number"
msgstr ""

#: ../qml/Components/GenerateInputForSettings.qml:82
msgid "String"
msgstr ""

#: ../qml/Components/GenerateInputForSettings.qml:103
msgid "Boolen"
msgstr ""

#: ../qml/Components/GenerateInputForSettings.qml:125
msgid "Select Value"
msgstr ""

#: ../qml/Components/UI/SharePage.qml:10
msgid "Select App to share with"
msgstr ""

#: ../qml/Main.qml:99
msgid "Rsses"
msgstr ""

#: ../qml/Main.qml:103 ../qml/Pages/Information.qml:51
#: darkeye.ursses.desktop.in.h:1
msgid "uRsses"
msgstr ""

#: ../qml/Main.qml:115 ../qml/Pages/AddRssPage.qml:45
#: ../qml/Pages/ManageFeedsPage.qml:20
msgid "Add Feed"
msgstr ""

#: ../qml/Main.qml:122 ../qml/Pages/SettingsPage.qml:14
msgid "Settings"
msgstr ""

#: ../qml/Main.qml:130
msgid "Ascending"
msgstr ""

#: ../qml/Main.qml:130
msgid "Descending"
msgstr ""

#: ../qml/Main.qml:130
msgid "Toggle Sort (%1) "
msgstr ""

#: ../qml/Main.qml:196 ../qml/Main.qml:228
msgid "Added Feed : %1"
msgstr ""

#: ../qml/Pages/AddRssPage.qml:16
msgid "Add new Feed"
msgstr ""

#: ../qml/Pages/AddRssPage.qml:34
msgid "Enter URL or a domain to search feeds for"
msgstr ""

#: ../qml/Pages/AddRssPage.qml:45
msgid "Search"
msgstr ""

#: ../qml/Pages/EnteryViewPage.qml:28
msgid "Next story"
msgstr ""

#: ../qml/Pages/EnteryViewPage.qml:37
msgid "Previous story"
msgstr ""

#: ../qml/Pages/EnteryViewPage.qml:47
msgid "Open in browser"
msgstr ""

#: ../qml/Pages/EnteryViewPage.qml:54
msgid "Show Content"
msgstr ""

#: ../qml/Pages/EnteryViewPage.qml:54
msgid "Show Web"
msgstr ""

#: ../qml/Pages/EnteryViewPage.qml:66
msgid "Save/Bookmark"
msgstr ""

#: ../qml/Pages/EnteryViewPage.qml:74 ../qml/Pages/ManageBookmarksPage.qml:70
msgid "Share"
msgstr ""

#: ../qml/Pages/EnteryViewPage.qml:185
msgid "Web Config"
msgstr ""

#: ../qml/Pages/EnteryViewPage.qml:201
msgid "Zoom"
msgstr ""

#: ../qml/Pages/Information.qml:9
msgid "App Information"
msgstr ""

#: ../qml/Pages/Information.qml:17
msgid "Get the source"
msgstr ""

#: ../qml/Pages/Information.qml:18
msgid "Report issues"
msgstr ""

#: ../qml/Pages/Information.qml:19
msgid "Contributors"
msgstr ""

#: ../qml/Pages/Information.qml:21
msgid "Domain Feed search provided by 'Feedsearch' "
msgstr ""

#: ../qml/Pages/Information.qml:22
msgid "Donate"
msgstr ""

#: ../qml/Pages/Information.qml:57
msgid "Version %1"
msgstr ""

#: ../qml/Pages/MainFeedPage.qml:157
msgid "Loading feeds...."
msgstr ""

#: ../qml/Pages/MainFeedPage.qml:158
msgid ""
"No Feeds yet... You can add feeds by clicking on the '+' sign in the top "
"right."
msgstr ""

#: ../qml/Pages/MainFeedPage.qml:252 ../qml/Pages/ManageBookmarksPage.qml:18
msgid "Bookmarks"
msgstr ""

#: ../qml/Pages/ManageAccountsPage.qml:18
msgid "Nextcloud Accounts"
msgstr ""

#: ../qml/Pages/ManageAccountsPage.qml:23
msgid "Add a new account"
msgstr ""

#: ../qml/Pages/ManageAccountsPage.qml:29
msgid "Synchronize Now"
msgstr ""

#: ../qml/Pages/ManageAccountsPage.qml:34
msgid "Synchronization started"
msgstr ""

#: ../qml/Pages/ManageAccountsPage.qml:38
msgid "Upload local changes to NextCloud"
msgstr ""

#: ../qml/Pages/ManageAccountsPage.qml:46
msgid "NextCloud news feeds will be merged and updated with the local feeds."
msgstr ""

#: ../qml/Pages/ManageAccountsPage.qml:47
msgid ""
"Local changes to the feed list will not be uploaded to NextCloud "
"automatically."
msgstr ""

#: ../qml/Pages/ManageAccountsPage.qml:67
msgid "Authentication error"
msgstr ""

#: ../qml/Pages/ManageAccountsPage.qml:76
msgid ""
"Authenticated. Currently feeds can only be retrived from the cloud not "
"uploaded or updated."
msgstr ""

#: ../qml/Pages/ManageAccountsPage.qml:90
msgid ""
"No Nextcloud accounts available. Tap on the button at the top right to add "
"an account."
msgstr ""

#: ../qml/Pages/ManageAccountsPage.qml:112
msgid "Select an account to use"
msgstr ""

#: ../qml/Pages/ManageBookmarksPage.qml:24
msgid "No bookmarks yet..."
msgstr ""

#: ../qml/Pages/ManageBookmarksPage.qml:59 ../qml/Pages/ManageFeedsPage.qml:46
msgid "Delete"
msgstr ""

#: ../qml/Pages/ManageBookmarksPage.qml:78
msgid "Open Externally"
msgstr ""

#: ../qml/Pages/ManageFeedsPage.qml:15
msgid "Manage Feeds"
msgstr ""

#: ../qml/Pages/SettingsPage.qml:18
msgid "Accounts"
msgstr ""

#: ../qml/Pages/SettingsPage.qml:25
msgid "Feeds"
msgstr ""

#: ../qml/Pages/SettingsPage.qml:32
msgid "About"
msgstr ""

#: ../qml/Pages/SettingsPage.qml:66
msgid "Show content instead of linked page"
msgstr ""

#: ../qml/Pages/SettingsPage.qml:67
msgid ""
"Shoud we just show the feed entry content instead of navigating to the "
"related website"
msgstr ""

#: ../qml/Pages/SettingsPage.qml:70
msgid "Show section name"
msgstr ""

#: ../qml/Pages/SettingsPage.qml:71
msgid "Seperates entries by Sections"
msgstr ""

#: ../qml/Pages/SettingsPage.qml:74
msgid "Item to load per feed"
msgstr ""

#: ../qml/Pages/SettingsPage.qml:75
msgid "How much enteries to load per feed source"
msgstr ""

#: ../qml/Pages/SettingsPage.qml:78
msgid "Sorting field name"
msgstr ""

#: ../qml/Pages/SettingsPage.qml:79
msgid "What form the feed entry field will be used for sorting."
msgstr ""

#: ../qml/Pages/SettingsPage.qml:82
msgid "Section field name"
msgstr ""

#: ../qml/Pages/SettingsPage.qml:83
msgid " "
msgstr ""

#: ../qml/Pages/SettingsPage.qml:86
msgid "Web browser default zoom"
msgstr ""

#: ../qml/Pages/SettingsPage.qml:87
msgid "The zoom value to set the internal web browser to."
msgstr ""

#: ../qml/Pages/SettingsPage.qml:90
msgid "Update feeds every"
msgstr ""

#: ../qml/Pages/SettingsPage.qml:91
msgid "The amount of time (in minutes) to wait between automatic feeds update."
msgstr ""

#: ../qml/Pages/SettingsPage.qml:94
msgid "Open feeds URL Externally"
msgstr ""

#: ../qml/Pages/SettingsPage.qml:95
msgid ""
"Open feeds URL using external browser instead of using the internal browser "
"view."
msgstr ""

#: ../qml/Pages/SettingsPage.qml:98
msgid "Swipe next/Previous story"
msgstr ""

#: ../qml/Pages/SettingsPage.qml:99
msgid "Move between sotries using swipe to chage to the next/previous story."
msgstr ""
