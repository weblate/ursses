
import QtQuick 2.9
import Ubuntu.Components 1.3
import QtQuick.Controls 2.2 as QControls
import Ubuntu.OnlineAccounts 2.0
import Ubuntu.Components.Popups 1.3
import Ubuntu.OnlineAccounts 2.0

import "../Components"
import "../Components/UI"

Page {
	id:_manageAccounts
	opacity:accounts.ready ?  1 : 0.5
	enabled:accounts.ready
	header: PageHeaderWithBottomText {
		id:header
		title:i18n.tr("Nextcloud Accounts")

		trailingActionBar {
			actions: [
				Action {
					name: i18n.tr("Add a new account")
					iconName:"contact-new"
					enabled:accounts.ready
					onTriggered: accounts.requestAccess(accounts.applicationId + "_nextcloud", {})
				},
				Action {
					name: i18n.tr("Synchronize Now")
					iconName:"reload"
					enabled:accounts.ready &&  appSettings.nextCloudCreds.accountId
					onTriggered: {
						root.syncNextCloud();
						header.message.text = i18n.tr("Synchronization started");
					}
				},
				Action {
					name: i18n.tr("Upload local changes to NextCloud")
					iconName:"merge"
					enabled:accounts.ready &&  appSettings.nextCloudCreds.accountId
					visible:false
					checked: true == appSettings.nextCloudCreds.uploadChanges 
					checkable:true
					onTriggered: {
						appSettings.nextCloudCreds.uploadChanges = checked
						header.message.text = checked ?  i18n.tr("NextCloud news feeds will be merged and updated with the local feeds.") :
														 i18n.tr("Local changes to the feed list will not be uploaded to NextCloud automatically.")
					}
				}
			]
		}
	}
	
   //Add Online Account connection
	AccountModel {
		id: accounts
		property var host: null;
		onAccessReply : if (reply.errorCode) {
			console.log("Failed to obtain access to nectcloud got error : "+reply.errorText);
			console.log(JSON.stringify(reply));
			header.message.text = i18n.tr("Failed to obtain access to nectcloud got error : %1").args(reply.errorText);
		} else {
			_manageAccounts.useAccount(reply.account);
		}
	}
	Connections {
		id: accountConnection
		target: null
		onAuthenticationReply: {
			var reply = authenticationData

			if ("errorCode" in reply) {
				console.warn("Authentication error: " + reply.errorText + " (" + reply.errorCode + ")")
				header.message.text = i18n.tr("Authentication error");
			} else {
				//appSettings.nextCloudCreds.user = reply.Username
				//appSettings.nextCloudCreds.pass = reply.Password
				appSettings.nextCloudCreds.host = accountConnection.target.settings.host
				appSettings.nextCloudCreds.accountId = accountConnection.target.accountId
				appSettings.nextCloudCreds.encodedCreds = Qt.btoa(reply.Username+":"+reply.Password)
				appSettings.nextCloudCreds = appSettings.nextCloudCreds;
				header.message.text = i18n.tr("Authenticated. Feeds will be retrived from the cloud.");
			}
		}

	}
	Label {
		z:1;
		anchors {
			left:parent.left
			right:parent.right
			verticalCenter:parent.verticalCenter
			margins: units.gu(2)
		}
		visible: accounts.count === 0
		text: i18n.tr("No Nextcloud accounts available. Tap on the button at the top right to add an account.")
		wrapMode: Text.Wrap
		horizontalAlignment: Text.AlignHCenter
	}
	ListView {
		anchors {
			top:header.bottom
			left:parent.left
			right:parent.right
			bottom:parent.bottom
		}
		id: accountList
		visible: accounts.count !== 0
		enabled:accounts.ready
		
		model: accounts
		
		header: Label {
			anchors {
				left:parent.left
				right:parent.right
			}
			height:units.gu(5)
			text: i18n.tr("Select an account to use")
		}
		
		delegate: ListItem {
			anchors { 
				left: parent.left; 
				right: parent.right;
			}
			height: units.gu(8)
			ListItemLayout {
				title.text: model.displayName
				Icon {
					name:"tick"
					height:units.gu(3)
					width:height
					visible: model.accountId == appSettings.nextCloudCreds.accountId
					SlotsLayout.position: SlotsLayout.Trailing;
				}
				//Icon {
					//name: appSettings.nextCloudCreds.fullSync ? "sync" : "document-save"
					//height:units.gu(3)
					//width:height
					//visible: appSettings.nextCloudCreds.fullSync
					//SlotsLayout.position: SlotsLayout.Trailing;
				//}
			}
			onClicked: {
				useAccount(model.account)
			}
		}

	}
	
	//----------------------------- Functions ------------------------


	function useAccount(account) {
		accounts.host = account.settings.host
		accountConnection.target = account
		account.authenticate({host:account.settings.host,accountId:account.accountId})
	}

}

/*
 * Copyright (C) 2021  Eran DarkEye Uzan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * darkeye.ursses is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

